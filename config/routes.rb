Rails.application.routes.draw do
  require 'sidekiq/web'
  mount Sidekiq::Web, at: '/sidekiq'

  devise_for :users, controllers: {
    registrations: "users/registrations",
    confirmations: "users/confirmations",
    sessions: "users/sessions",
    passwords: "users/passwords"
  }

  devise_scope :user do
    get 'lock_user', to: 'users/sessions#lock_user'
    post 'unlock_user', to: 'users/sessions#unlock_user'
  end

  resources :business_clients, except: [:show, :destroy, :edit], path: 'clients', as: 'clients' do

    get :autocomplete, on: :collection

    resources :business_companies, path: 'companies', only:[:show, :update] do

      get :preferences, on: :member

      namespace 'bc' do
        resources :departments
        resources :branches
        resources :medical_clinics
      end

      resources :dashboard, only: [:index]
      resources :claimants
    end
  end

  root to: 'home#index'
  get 'dashboard', to: 'users/dashboard#index', as: :dashboard
  get '*unmatched_route', :to => 'home#routing_error'

end
