class BusinessCompanyDecorator < Draper::Decorator
  delegate_all

  def company_name
    name.camelize
  end
end
