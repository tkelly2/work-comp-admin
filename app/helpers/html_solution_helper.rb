module HtmlSolutionHelper

  def is_sidebar_closed?
    params[:controller] == "business_companies" && params[:action] == "show" || params[:controller] == "users/registrations" && params[:action] == "edit"
  end

  def get_alert_class(name)
    name = name.to_s
    return "alert-warning" if name == 'warning'
    return "alert-success" if name == 'success' || name == 'notice'
    return "alert-danger" if name == 'error' || name == 'danger' || name == 'alert'
    return "alert-info" if name == 'info'
  end

  def get_root_path
    if resource.has_role? :owner
      authenticated_root_path
    elsif resource.type == 'BusinessClient'
      dashboard_path
    else
      unauthenticated_root_path
    end
  end

  def is_dashboard?
    params[:controller] == "users/dashboard" && params[:action] == "index"
  end

end
