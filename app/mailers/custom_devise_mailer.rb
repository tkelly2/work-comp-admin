class CustomDeviseMailer < Devise::Mailer
  default from: 'from@example.com'
  helper :application

  def reset_password_instructions(record, token, opts={})
    @token = token
    devise_mail(record, :reset_password_instructions, opts)
  end

  def confirmation_instructions(record, token, opts={})
    @return_url = opts[:return_url]
    @token = token
    devise_mail(record, :confirmation_instructions, opts)
  end


  def confirmation_email(user, opts={})
    @token = user.confirmation_token
    devise_mail(user, :confirmation_instructions, opts)
  end

end