class BusinessClientsController < ApplicationController
  load_and_authorize_resource
  layout "dashboard"
  before_action :handle_response, only: [:create]
  before_action :initialize_business_companies, only: [:create, :index, :autocomplete]
  before_action :initialize_business_client_and_company, :check_and_initialize_params, only: [:update]
  autocomplete :business_company, :name

  def new
    common_attributes
  end

  def autocomplete
    bcs = @business_companies.search_by_name(params[:term].downcase)
    render json: bcs.map{ |bc| bc.name.camelize }
  end

  def create
    business_client = CreateBusinessClientService.new(business_client_params)
    business_client, result, msg = business_client.create
    if result
      flash[:notice] = "Client has been created successfully."
    else
      flash[:error] = msg
    end
  end

  def index
    @business_companies = @business_companies.search_by_name(params[:search]) if params[:search].present?
    @business_companies = @business_companies.paginate(page: params[:page], per_page: 10)
  end

  def update
    if @business_client.send(@update_method, @client_data)
      flash[:notice] = "Client has been updated successfully."
    else
      flash[:error] = @business_client.errors.full_messages.join("<br/>").html_safe
    end
  end

  private

  def initialize_business_client_and_company
    @business_client = BusinessClient.where(id: params[:id]).first
    @business_company = @business_client.business_companies.where(id: params[:business_company_id]).first
    if @business_client.blank? && @business_company.blank?
      flash[:error] = "Client is not found."
      redirect_to dashboard_path
    end
  end

  def handle_response
    respond_to do |format|
      format.html{ redirect_to root_path }
      format.js{ }
    end
  end

  def business_client_params
    params.require(:business_client).permit(:name, :first_name, :last_name, :phone, :email,
      business_companies_attributes: [
        :name, :phone, :notepad, :broker_of_record, :who_to_bill, :payment_options,
        payment_profiles_attributes:[:card_name, :card_number, :card_expiry_date, :token],
        address_attributes: [:address, :city, :state, :country, :zip]
      ]
    )
  end

  def check_and_initialize_params
    @update_method, @client_data =  "update_attributes", business_client_account_params
    if @client_data[:password].present?
      flash[:error] = "Password does not match the confirmation password." and return unless @client_data[:password] == @client_data[:password_confirmation]
    else
      @client_data.delete(:password)
      @client_data.delete(:password_confirmation)
      @update_method = "update_without_password"
    end
  end

  def business_client_account_params
    params.require(:business_client).permit(:first_name, :last_name, :phone, :password, :password_confirmation)
  end

  def common_attributes
    @business_client = BusinessClient.new
    business_company = @business_client.business_companies.build
    business_company.build_address
    business_company.payment_profiles.build
  end

end
