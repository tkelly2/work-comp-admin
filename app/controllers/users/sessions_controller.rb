class Users::SessionsController < Devise::SessionsController
  layout :layout
  skip_before_action :require_no_authentication, only: :new
  skip_before_action :unlock_screen

  def new
    redirect_to dashboard_url and return if current_user
    flash[:error] = "Please enable cookies to sign in." if request.cookies.blank?
    super
  end

  def create
    self.resource = warden.authenticate!(auth_options)
    set_flash_message(:notice, :signed_in) if is_navigational_format?
    sign_in(resource_name, resource)
    respond_with resource, location: dashboard_url
  end

  def lock_user
    if current_user.present?
      current_user.lock! unless current_user.locked?
    else
      redirect_to new_user_session_url
    end
  end

  def unlock_user
    if current_user.present? && params[:user].present?
      if current_user.valid_password? params[:user][:password]
        current_user.unlock!
        redirect_to dashboard_url
      else
        flash[:error] = "Please enter a valid password."
        redirect_to lock_user_url
      end
    end
  end

  def destroy
    current_user.unlock! if current_user.present?
    super
  end

  private

  def layout
    if params[:action] == 'lock_user' || params[:action] == 'unlock_user'
      'lock_user'
    else
      'registration'
    end
  end

end
