class Users::DashboardController < ApplicationController
  before_action :initialize_business_companies, :initialize_user_dashboard
  layout "dashboard"

  def index
    respond_to do |format|
      format.html{}
      format.js{}
    end
  end

  private

  def initialize_user_dashboard
    if current_user.has_role? :client
      @business_companies = @business_companies.paginate(page: params[:page], per_page: 10)
      @page_path = 'business_clients/index'
    else
      @page_path = 'users/dashboard/dashboard'
    end
  end
end
