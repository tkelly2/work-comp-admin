class Users::RegistrationsController < Devise::RegistrationsController
  layout :layout
  before_action :handle_response, :check_and_initialize_params, only: [:update]

  def new
    @user = build_resource
    @user.build_address
  end

  def create
    build_resource(user_params)
    begin
      User.transaction do
        if resource.save
          set_flash_message :notice, :signed_up
          sign_up(resource_name, resource)
          respond_with resource, location: root_path
        else
          clean_up_passwords resource
          flash[:error] = resource.errors.full_messages.join("<br/>").html_safe
          render 'new'
        end
      end
    rescue Exception => e
      clean_up_passwords resource
      flash[:error] = e.message.html_safe
      redirect_to new_user_registration_path
    end
  end

  def edit
    @user = @user.becomes(User)
    @user.build_image if @user.image.blank?
  end

  def update
    return if flash[:error].present?
    if @user.send(@update_method, @user_data)
      sign_in(@user, :bypass => true)
      flash[:notice] = "User has been updated successfully."
      initialize_user
    else
      flash[:error] = @user.errors.full_messages.join("<br/>").html_safe
    end
  end

  private

  def user_params
    params.require(:user).permit(:first_name, :last_name, :phone, :email, :password, :current_password, :password_confirmation, :username, address_attributes: [:address, :city, :state, :country, :zip], image_attributes: [:id, :image])
  end

  def layout
    params[:action] == 'edit' ? 'dashboard' : 'registration'
  end

  def check_and_initialize_params
    @update_method, @user_data =  "update_attributes", user_params
    if @user_data[:password].present?
      flash[:error] = "Your current password is incorrect." and return unless @user.valid_password?(@user_data[:current_password])
      flash[:error] = "Password does not match the confirmation password." and return unless @user_data[:password] == @user_data[:password_confirmation]
    else
      @user_data.delete(:password)
      @user_data.delete(:password_confirmation)
      @update_method = "update_without_password"
    end
  end

  def handle_response
    respond_to do |format|
      format.html{ redirect_to dashboard_path }
      format.js{ }
    end
  end

  def initialize_user
    @user = @user.reload
    @user = @user.becomes(User)
    @user.build_image if @user.image.blank?
    if user_params[:image_attributes].present?
      @image = @user.image.try(:image_url)
    end
  end

end
