class Bc::DepartmentsController < Bc::BaseController
  before_action :initialize_department, only: [:edit, :update, :destroy]

  def index
    init_departments
  end

  def new
    @department = @business_company.departments.new
  end

  def edit
  end

  def create
    @department = @business_company.departments.new(department_params)
    if @department.save
      flash[:notice] = "Department has been created successfully."
    else
      flash[:error] = @department.errors.full_messages.join("<br/>").html_safe
    end
    init_departments
  end

  def update
    if @department.update_attributes(department_params)
      flash[:notice] = "Department has been updated successfully."
    else
      flash[:error] = @department.errors.full_messages.join("<br/>").html_safe
    end
    init_departments
  end

  def destroy
    if @department.destroy && @department.destroyed?
      flash[:notice] = "Department has been deleted successfully."
    else
      flash[:error] = @department.errors.full_messages.join("<br/>").html_safe
    end
    init_departments
  end

  private

  def initialize_department
    @department = @business_company.departments.where(id: params[:id]).first
    flash[:error] = "Department is not found." if @department.blank?
  end

  def department_params
    params.require(:department).permit(:name)
  end

end
