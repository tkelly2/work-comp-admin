class Bc::BaseController < ApplicationController
  before_action :handle_response, :initialize_business_client_and_company

  ['branches', 'departments', 'medical_clinics'].each do |name|
    define_method "init_#{name}" do
      instance_variable_set("@#{name}", eval("@business_company.#{name}.created_at_desc"))
    end
  end

  private

  def handle_response
    respond_to do |format|
      format.html{ redirect_to dashboard_path }
      format.js{ }
    end
  end

  def initialize_business_client_and_company
    @business_client = BusinessClient.where(id: params[:client_id]).first
    @business_company = @business_client.business_companies.where(id: params[:business_company_id]).first if @business_client.present?
    if @business_client.blank? && @business_company.blank?
      flash[:error] = "Client is not found."
      redirect_to dashboard_path
    end
  end

end
