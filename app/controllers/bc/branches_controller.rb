class Bc::BranchesController < Bc::BaseController
  before_action :initialize_branch, only: [:edit, :update, :destroy]

  def index
    init_branches
  end

  def new
    @branch = @business_company.branches.new
  end

  def edit
  end

  def create
    @branch = @business_company.branches.new(branch_params)
    if @branch.save
      flash[:notice] = "Branch has been created successfully."
    else
      flash[:error] = @branch.errors.full_messages.join("<br/>").html_safe
    end
    init_branches
  end

  def update
    if @branch.update_attributes(branch_params)
      flash[:notice] = "Branch has been updated successfully."
    else
      flash[:error] = @branch.errors.full_messages.join("<br/>").html_safe
    end
    init_branches
  end

  def destroy
    if @branch.destroy && @branch.destroyed?
      flash[:notice] = "Branch has been deleted successfully."
    else
      flash[:error] = @branch.errors.full_messages.join("<br/>").html_safe
    end
    init_branches
  end

  private

  def initialize_branch
    @branch = @business_company.branches.where(id: params[:id]).first
    flash[:error] = "Branch is not found." if @branch.blank?
  end

  def branch_params
    params.require(:branch).permit(:name)
  end

end
