class Bc::MedicalClinicsController < Bc::BaseController
  before_action :initialize_medical_clinic, only: [:edit, :update, :destroy]

  def index
    init_medical_clinics
  end

  def new
    @medical_clinic = @business_company.medical_clinics.new
  end

  def edit
  end

  def create
    @medical_clinic = @business_company.medical_clinics.new(medical_clinic_params)
    if @medical_clinic.save
      flash[:notice] = "Medical clinic has been created successfully."
    else
      flash[:error] = @medical_clinic.errors.full_messages.join("<br/>").html_safe
    end
    init_medical_clinics
  end

  def update
    if @medical_clinic.update_attributes(medical_clinic_params)
      flash[:notice] = "Medical clinic has been updated successfully."
    else
      flash[:error] = @medical_clinic.errors.full_messages.join("<br/>").html_safe
    end
    init_medical_clinics
  end

  def destroy
    if @medical_clinic.destroy && @medical_clinic.destroyed?
      flash[:notice] = "Medical clinic has been deleted successfully."
    else
      flash[:error] = @medical_clinic.errors.full_messages.join("<br/>").html_safe
    end
    init_medical_clinics
  end

  private

  def initialize_medical_clinic
    @medical_clinic = @business_company.medical_clinics.where(id: params[:id]).first
    flash[:error] = "Medical clinic is not found." if @medical_clinic.blank?
  end

  def medical_clinic_params
    params.require(:medical_clinic).permit(:name)
  end

end
