class HomeController < ActionController::Base
  layout "application"
  def index
  end

  def routing_error
    redirect_to root_url
  end


end
