class BusinessCompaniesController < ApplicationController
  layout "dashboard"
  before_action :handle_response, only: [:update]
  before_action :initialize_business_companies, :initialize_business_company

  def show
  end

  def update
    if @business_company.update_attributes(business_company_params)
      flash[:notice] = "Client has been updated successfully."
    else
      flash[:error] = @business_company.errors.full_messages.join("<br/>")
    end
  end

  def preferences
  end

  private

  def initialize_business_company
    @business_company = @business_companies.where(id: params[:id]).first if @business_companies.present?
    if @business_company.blank?
      flash[:error] = "client is not found."
      redirect_to dashboard_path
    end
  end

  def handle_response
    respond_to do |format|
      format.html{ redirect_to dashboard_path }
      format.js{ }
    end
  end

  def business_company_params
    params.require(:business_company).permit(:name, :phone, :policy_number, :policy_year, :medical_clinic, :insurance_company, :notepad, :broker_of_record, :who_to_bill, :payment_options, payment_profiles_attributes:[:card_name, :card_number, :card_expiry_date, :token], address_attributes: [:address, :city, :state, :country, :zip], bc_tasks_attributes: [:task_id, :id, :active])
  end


end
