class ApplicationController < ActionController::Base
  protect_from_forgery with: :exception
  before_action :authenticate_user!, :unlock_screen

  def initialize_business_companies
    if current_user.is_owner?
      @business_companies = BusinessCompany.all
    else
      @business_companies = current_user.business_companies
    end
  end

  def current_ability
    @current_ability ||= Ability.new(current_user)
  end

  rescue_from CanCan::AccessDenied do |exception|
    flash[:error] = exception.message
    respond_to do |format|
      format.html { redirect_to dashboard_url }
      format.js { render js: "window.location.href = '#{dashboard_url}'" }
    end
  end

  def after_sign_out_path_for(resource_or_scope)
    new_user_session_path
  end

  private

  def unlock_screen
    if current_user.present? && current_user.locked?
      redirect_to lock_user_path and return
    end
  end
end
