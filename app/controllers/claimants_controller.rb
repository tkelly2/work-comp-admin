class ClaimantsController < ApplicationController
  before_action :handle_response
  before_action :initialize_business_companies, :initialize_business_company

  def new

  end

  private

  def initialize_business_company
    @business_company = @business_companies.where(id: params[:business_company_id]).first if @business_companies.present?
    if @business_company.blank?
      flash[:error] = "client is not found."
      redirect_to dashboard_path
    end
  end

  def handle_response
    respond_to do |format|
      format.html{ redirect_to dashboard_path }
      format.js{ }
    end
  end
end
