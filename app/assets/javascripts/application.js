//Global-begin
//= require jquery.min
//= require jquery_ujs
//= require jquery-ui
//= require autocomplete-rails
//= require bootstrap.min
//= require js.cookie.min
//= require bootstrap-hover-dropdown.min
//= require jquery.slimscroll.min
//= require jquery.blockui.min
//= require jquery.uniform.min
//= require bootstrap-switch.min

//= require iziToast
//= require jquery_nested_form
//Global-end
