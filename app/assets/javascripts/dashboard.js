//= require application
//= require bootstrap-datepicker.min
//= require bootstrap-datetimepicker.min
//= require bootstrap-fileinput
//= require select2.full.min
//= require jquery.multi-select
//= require sweetalert.min
//= require datatable
//= require datatables.min
//= require datatables.bootstrap
//= require jquery.sparkline.min
//= require app.min
//= require profile.min
//= require table-datatables-managed.min
//= require todo.min
//= require components-date-time-pickers.min
//= require ui-sweetalert.min
//= require jquery.validate.min
//= require common
//= require additional-methods.min
//= require jquery.bootstrap.wizard.min
//= require form-wizard.min
//= require layout.min
//= require demo.min

//= require quick-sidebar.min

