$(document).ajaxStart(function() {
  block_ui();
}).ajaxStop(function() {
  $.unblockUI();
}).ajaxSuccess(function() {
  $.unblockUI();
});

$(document).on('click', 'a[data-remote="true"]', function(event){
  event.stopImmediatePropagation();
});

function block_ui(){
  $.blockUI({
    css: {
      border: 'none',
      padding: '15px',
      backgroundColor: 'transparent',
      '-webkit-border-radius': '10px',
      '-moz-border-radius': '10px'
    },
    overlayCSS:  { opacity:0.8},
    message: '<img src="/assets/c.svg" style="width: 80px;"/>'
  });
}

function validate_business_company(form){
  form.validate({
    doNotHideMessage: true, //this option enables to show the error/success messages on tab switch.
    errorElement: 'span', //default input error message container
    focusInvalid: true, // do not focus the last invalid input
    rules: {
      "business_company[name]": {
        required: true
      },
      "business_company[email]": {
        required: true,
        email: true
      },
      "business_company[phone]": {
        required: true
      },
      "business_company[address_attributes][address]": {
        required: true
      },
      "business_company[address_attributes][city]": {
        required: true
      },
      "business_company[address_attributes][state]": {
        required: true
      },
      "business_company[address_attributes][country]": {
        required: true
      },
      "business_company[address_attributes][zip]": {
        required: true
      },
      //payment
      "business_company[who_to_bill]": {
        required: true,
      }
    }
  });
}

function validate_business_client(form){
  form.validate({
    doNotHideMessage: true, //this option enables to show the error/success messages on tab switch.
    errorElement: 'span', //default input error message container
    focusInvalid: true, // do not focus the last invalid input
    rules: {
      "business_client[first_name]": {
        required: true
      },
      "business_client[last_name]": {
        required: true,
      },
      "business_client[phone]": {
        required: true
      },
      "business_client[password]": {
        minlength: 8
      },
      "business_client[password_confirmation]": {
        minlength: 8,
        equalTo: "#business_client_password"
      }
    }
  });
}


$.validator.addMethod('filesize', function (value, element, param) {
  return this.optional(element) || (element.files[0].size <= param)
}, 'File size must be less than {0}');

function validate_edit_user(form){
  form.validate({
    ignore: [],
    doNotHideMessage: true, //this option enables to show the error/success messages on tab switch.
    errorElement: 'span', //default input error message container
    focusInvalid: true, // do not focus the last invalid input
    rules: {
      "user[first_name]": {
        required: true
      },
      "user[last_name]": {
        required: true
      },
      "user[phone]": {
        required: true
      },
      "user[image_attributes][image]": {
        required: true,
        extension: "jpg|png|jpeg",
        filesize: 4194304
      },
      "user[current_password]": {
        required: true,
        minlength: 8
      },
      "user[password]": {
        required: true,
        minlength: 8
      },
      "user[password_confirmation]": {
        required: true,
        minlength: 8,
        equalTo: "#user_password"
      }
    },
    messages: {
      "user[image_attributes][image]": {
        extension: 'You are only allowed to upload "jpg, jpeg, png" files.',
        required: "Please upload file.",
        filesize:" File size must be less than 4 MB.",
      }
    }
  });
}

$(document).ready(function(){
  $('#all_clients .pagination a').attr('data-remote', 'true');
});

$(document).on('click', '.form_cancel_btn', function(){
  $(".main_overview_link").click();$(window).scrollTop(0);
});

$(document).on('change', '#preferences_select', function(){
  var path = $(this).val();
  $.ajax({
    type: 'get',
    dataType: 'script',
    url: path
  });
});