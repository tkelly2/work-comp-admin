class PaymentProfile < ApplicationRecord
  attr_accessor :card_cvc
  belongs_to :business_company
  validates :card_name, :card_number, :card_expiry_date, :token, presence: true
end
