class BusinessCompany < ApplicationRecord

  belongs_to :business_client
  has_many :employees
  has_many :branches
  has_many :departments
  has_many :medical_clinics
  has_many :bc_tasks
  has_many :payment_profiles, dependent: :destroy

  validates :name, presence: true, length: {maximum: 110}
  validates :phone, presence: true, length: {maximum: 18}, format: { with: /\A[+]{0,1}[\s\/\(\)\-\.\d]{10,15}\z/ }

  accepts_nested_attributes_for :branches, allow_destroy: true
  accepts_nested_attributes_for :departments, allow_destroy: true
  accepts_nested_attributes_for :payment_profiles, allow_destroy: true
  accepts_nested_attributes_for :bc_tasks, allow_destroy: true

  after_create :build_tasks

  include DateTimeParser
  include WcaAddressable

  scope :search_by_name, -> (keyword){ where("lower(name) like ? ", "%#{keyword.downcase}%") }

  WHO_TO_BILL = { 'Company': 1, 'Broker': 2, 'Insurance Company': 3 }
  PAYMENT_OPTIONS = { 'Auto-Pay with this Credit Card': 1, 'Email me monthly billing': 2 }

  def client
    business_client
  end

  def contact_person_email
    client.email
  end

  def contact_person_phone
    client.phone
  end

  def contact_person_name
    client.full_name
  end

  private

  def build_tasks
    Task.all.each{ |task| self.bc_tasks.create(task_id: task.id) }
  end

end
