class BcTask < ApplicationRecord
  belongs_to :business_company
  belongs_to :task

  #before_save :custom_validation

  def name
    task.name
  end
end
