class Ability
  include CanCan::Ability

  def initialize(user)
    if user.has_role? :owner
      can :manage, :all
    elsif user.has_role? :client
      cannot :manage, [:business_clients]
      can [:update], BusinessClient, id: user.id
    end
  end
end
