class Task < ApplicationRecord

  # validates :description, presence: true
  # validates :task_date, timeliness: { type: :date }
  # validates :completion_date, timeliness: { type: :date }, allow_nil: true
  has_many :bc_tasks
  scope :pending_task, -> { where("is_pending = ? AND task_date <= ?", true, Date.today) }
  scope :order_by_task_date, -> {order("completion_date DESC, task_date ASC, created_at DESC")}


end
