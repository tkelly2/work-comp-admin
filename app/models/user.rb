class User < ApplicationRecord
  rolify
  attr_accessor :terms_of_service, :name, :current_password
  include WcaAddressable
  include Imagable
  devise :database_authenticatable, :registerable, :confirmable, :recoverable, :rememberable, :trackable, :validatable

  validates :first_name, presence: true, length: {maximum: 110}
  validates :email, presence: true, length: {maximum: 200}, format: { with: Devise.email_regexp }
  validates_uniqueness_of :email, message: " is already registered with us. Please try reseting your password."
  validates :phone, presence: true, length: {maximum: 18}, format: { with: /\A[+]{0,1}[\s\/\(\)\-\.\d]{10,15}\z/ }

  def send_devise_notification(notification, *args)
    devise_mailer.send(notification, self, *args).deliver_later
  end

  def full_name
    "#{first_name.camelize}#{" "+last_name.camelize if last_name.present?}"
  end

  def role_type
    if self.roles.present?
      self.roles.first.name
    else
      self.type
    end
  end

  def is_owner?
    has_role? :owner
  end

  def lock!
    update_columns(locked: true)
  end

  def unlock!
    update_columns(locked: false)
  end

end
