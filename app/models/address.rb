class Address < ApplicationRecord
  belongs_to :addressable, polymorphic: true, optional: true

  validates :address, presence: true, length: {maximum: 255}
  validates :city, :state, presence: true, length: {maximum: 50}
  validates :zip, allow_blank: true, length: {maximum: 15}
end
