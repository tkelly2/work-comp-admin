class BusinessClient < User
  attr_accessor :name
  has_many :business_companies
  accepts_nested_attributes_for :business_companies

end