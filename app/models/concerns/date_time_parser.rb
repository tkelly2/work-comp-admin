module DateTimeParser
  extend ActiveSupport::Concern
  include ActionView::Helpers::DateHelper

  def joined_at
    wca_date_format self.created_at
  end

  def last_activity
    "#{time_ago self.updated_at} ago"
  end

  private

  def wca_date_format date
    date.strftime("%B %d, %Y") rescue nil
  end

  def time_ago date
    time_ago_in_words(date)
  end


end