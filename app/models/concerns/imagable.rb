module Imagable
  extend ActiveSupport::Concern

  included do
    has_one :image, as: :imageable
    accepts_nested_attributes_for :image, allow_destroy: true
  end

  def avatar
    image.image.medium.url if image.present?
  end

end