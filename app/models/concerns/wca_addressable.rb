module WcaAddressable
  extend ActiveSupport::Concern

  included do
    has_one :address, as: :addressable, dependent: :destroy
    accepts_nested_attributes_for :address, allow_destroy: true
  end

  def country
    self.address.try(:country)
  end

  def city
    self.address.try(:city)
  end

  def zip
    self.address.try(:zip)
  end

  def state
    self.address.try(:state)
  end

  def formatted_full_address
    full_address = ""
    full_address += "#{self.address.address}<br>" if self.address.address.present?
    full_address += " #{city}" if city.present?
    full_address += " #{state}" if state.present?
    full_address += " #{zip}<br>" if zip.present?
    full_address += " #{country}" if country.present?
    return full_address.html_safe
  end

  def full_address
    formatted_full_address.gsub("<br>", ",").gsub(",,", "")
  end

end