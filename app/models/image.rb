class Image < ApplicationRecord
  belongs_to :imageable, polymorphic: true, optional: true

  validates_size_of :image, maximum: 4.megabytes, message: "size should be less than 4MB"

  mount_uploader :image, ImageUploader
  process_in_background :image

end
