class CreateBusinessClientService
  require 'securerandom'

  def initialize(options={})
    @business_company_params = options[:business_companies_attributes]["0"]
    @business_client_params = business_client_params(options)
  end

  def create
    business_client = BusinessClient.new(@business_client_params)
    business_client, random_password = generate_password_and_skip_confirmation(business_client)
    business_client.business_companies.new(@business_company_params)
    if business_client.save
      business_client.add_role :client
      return business_client, true, ""
    else
      return business_client, false, business_client.errors.full_messages.join("<br/>")
    end
  end

  private

  def generate_password_and_skip_confirmation(business_client)
    random_password = SecureRandom.urlsafe_base64(8)
    business_client.password = business_client.password_confirmation = random_password
    business_client.skip_confirmation!
    return business_client, random_password
  end

  def business_client_params(params)
    first_name, last_name = params[:name].split(" ", 2) if params[:name].present?
    { first_name: first_name, last_name: last_name, phone: params[:phone], email: params[:email] }
  end

end