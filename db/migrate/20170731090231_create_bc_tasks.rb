class CreateBcTasks < ActiveRecord::Migration[5.1]
  def change
    create_table :bc_tasks do |t|
      t.integer :business_company_id
      t.integer :task_id
      t.boolean :active, default: false
      t.timestamps
    end
  end
end
