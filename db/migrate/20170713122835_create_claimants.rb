class CreateClaimants < ActiveRecord::Migration[5.1]
  def change
    create_table :claimants do |t|
      t.string :work_status
      t.string :follow_up_appointment
      t.string :name
      t.string :phone
      t.string :date_of_birth
      t.string :occupation
      t.string :last_day_worked
      t.float :body_mass_index
      t.string :claim_litigated
      t.string :start_date
      t.string :open_date
      t.string :closed_date
      t.string :claim_type
      t.string :body_part
      t.string :cause_of_loss
      t.string :type_of_injury
      t.string :work_status_report
      t.string :claim_number
      t.timestamps
    end
  end
end
