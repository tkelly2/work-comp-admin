class CreateTasks < ActiveRecord::Migration[5.1]
  def change
    create_table :tasks do |t|
      t.string :name
      t.date :task_date
      t.date :completion_date
      t.text :description
      t.boolean :is_pending
      t.integer :employee_id
      t.timestamps
    end
  end
end