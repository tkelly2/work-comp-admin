class CreatePaymentTransactions < ActiveRecord::Migration[5.1]
  def change
    create_table :payment_transactions do |t|
      t.boolean :payment_status, default: false
      t.string :response_message
      t.string :third_party_payment_id
      t.integer :payable_id
      t.string :payable_type
      t.integer :payment_method_id
      t.boolean :refund_status, default: false
      t.string :refund_transaction_id
      t.float :refunded_amount
      t.string :commission_transaction_id
      t.timestamps
    end
  end
end
