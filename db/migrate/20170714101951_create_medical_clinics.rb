class CreateMedicalClinics < ActiveRecord::Migration[5.1]
  def change
    create_table :medical_clinics do |t|
      t.integer :business_company_id
      t.string :name
      t.string :phone_number
      t.integer :contact_name
      t.integer :branch_id
      t.integer :user_id
      t.timestamps
    end
  end
end
