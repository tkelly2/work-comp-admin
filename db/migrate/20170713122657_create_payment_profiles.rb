class CreatePaymentProfiles < ActiveRecord::Migration[5.1]
  def change
    create_table :payment_profiles do |t|
      t.integer :business_company_id
      t.string :card_name
      t.string :card_number
      t.string :card_expiry_date
      t.string :token
      t.timestamps
    end
  end
end
