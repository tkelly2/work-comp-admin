class CreateAddresses < ActiveRecord::Migration[5.1]
  def change
    create_table :addresses do |t|
      t.integer :addressable_id
      t.string :addressable_type
      t.string :address
      t.string :city
      t.string :state
      t.string :zip
      t.string :country
      t.timestamps
    end
  end
end
