class CreateBusinessCompanies < ActiveRecord::Migration[5.1]
  def change
    create_table :business_companies do |t|
      t.string :name
      t.string :phone
      t.integer :business_client_id
      t.string :policy_number
      t.string :policy_year
      t.string :contact_person_name
      t.string :contact_person_email
      t.string :contact_person_phone
      t.text :notepad
      t.string :broker_of_record
      t.integer :who_to_bill
      t.integer :payment_options
      t.string :medical_clinic
      t.string :insurance_company
      t.timestamps
    end
  end
end
