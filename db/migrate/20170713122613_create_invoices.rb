class CreateInvoices < ActiveRecord::Migration[5.1]
  def change
    create_table :invoices do |t|
      t.integer :user_id
      t.string :number
      t.integer :status
      t.integer :payment_status
      t.float :price
      t.float :tax
      t.float :total_amount
      t.boolean :archive, default: false
      t.boolean :offline_paid, default: false
      t.timestamps
    end
  end
end
