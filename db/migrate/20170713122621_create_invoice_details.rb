class CreateInvoiceDetails < ActiveRecord::Migration[5.1]
  def change
    create_table :invoice_details do |t|
      t.integer :invoice_id
      t.integer :invoiceable_id
      t.string :invoiceable_type
      t.text :description
      t.float :tax
      t.float :price
      t.integer :quantity
      t.timestamps
    end
  end
end
