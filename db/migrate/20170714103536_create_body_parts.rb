class CreateBodyParts < ActiveRecord::Migration[5.1]
  def change
    create_table :body_parts do |t|
      t.string :name
      t.boolean :side_present, default: false
      t.integer :sort_order
      t.timestamps
    end
  end
end
