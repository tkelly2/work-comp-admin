#Create BodyPart
BodyPart.where(name: 'Head', sort_order: 1).first_or_create
BodyPart.where(name: 'Eyes', side_present: true, sort_order: 2).first_or_create
BodyPart.where(name: 'Ears', side_present: true, sort_order: 3).first_or_create
BodyPart.where(name: 'Nose', sort_order: 4).first_or_create
BodyPart.where(name: 'Mouth', sort_order: 5).first_or_create
BodyPart.where(name: 'Face', sort_order: 6).first_or_create
BodyPart.where(name: 'Throat', sort_order: 7).first_or_create
BodyPart.where(name: 'Chest', sort_order: 8).first_or_create
BodyPart.where(name: 'Abdomen', sort_order: 9).first_or_create
BodyPart.where(name: 'Genitals/Pelvis', sort_order: 10).first_or_create
BodyPart.where(name: 'Groin', sort_order: 11).first_or_create
BodyPart.where(name: 'Neck', sort_order: 12).first_or_create
BodyPart.where(name: 'Shoulder', side_present: true, sort_order: 13).first_or_create
BodyPart.where(name: 'Elbow', side_present: true, sort_order: 14).first_or_create
BodyPart.where(name: 'Wrist', side_present: true, sort_order: 15).first_or_create
BodyPart.where(name: 'Hand', side_present: true, sort_order: 16).first_or_create
BodyPart.where(name: 'Arm', side_present: true, sort_order: 17).first_or_create
BodyPart.where(name: 'Back', sort_order: 18).first_or_create
BodyPart.where(name: 'Hip', side_present: true, sort_order: 19).first_or_create
BodyPart.where(name: 'Knee', side_present: true, sort_order: 20).first_or_create
BodyPart.where(name: 'Ankle', side_present: true, sort_order: 21).first_or_create
BodyPart.where(name: 'Foot', side_present: true, sort_order: 22).first_or_create
BodyPart.where(name: 'Leg', side_present: true, sort_order: 23).first_or_create
BodyPart.where(name: 'Other', sort_order: 24).first_or_create

#Create Task
[
  'Notify Adjuster of New Status Report',
  'Notify Employer of Full Duty Release',
  'Notify Employer of Off Work Status',
  'Notify Employer of Light Duty Status',
  'Notify Adjuster of Missed Appointment',
  'Notify Adjuster of New Claim',
  'Missed Appointment Call',
  'Notify Employer of Missing Information Call',
  'Call For Medical Clinic Information',
  'Notify Employer of Missing Claimant Information Reminder',
  'Notify Employer of Missing Claimant Information'
].each { |task| Task.where(name: task).first_or_create }

#Create InjuryType
[
  'Strain/Sprain', 'Laceration', 'Burn', 'Bruise/Contusion', 'Abrasion/Puncture',
  'Amputation', 'Hearing Loss', 'Respiratory Condition', 'Heat Exhaustion/Frost Bite',
  'Allergic Reaction', 'Other'
].each{ |inj_type| InjuryType.where(name: inj_type).first_or_create }

user = User.new(first_name: 'nitin', last_name: 'verma', phone: "9098308915", email: "nititn1234gr@gmail.com", password: "password", password_confirmation: "password")
user.skip_confirmation!
user.add_role :owner
user.save!
