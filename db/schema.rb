# This file is auto-generated from the current state of the database. Instead
# of editing this file, please use the migrations feature of Active Record to
# incrementally modify your database, and then regenerate this schema definition.
#
# Note that this schema.rb definition is the authoritative source for your
# database schema. If you need to create the application database on another
# system, you should be using db:schema:load, not running all the migrations
# from scratch. The latter is a flawed and unsustainable approach (the more migrations
# you'll amass, the slower it'll run and the greater likelihood for issues).
#
# It's strongly recommended that you check this file into your version control system.

ActiveRecord::Schema.define(version: 20170731090231) do

  # These are extensions that must be enabled in order to support this database
  enable_extension "plpgsql"

  create_table "addresses", force: :cascade do |t|
    t.integer "addressable_id"
    t.string "addressable_type"
    t.string "address"
    t.string "city"
    t.string "state"
    t.string "zip"
    t.string "country"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

  create_table "bc_tasks", force: :cascade do |t|
    t.integer "business_company_id"
    t.integer "task_id"
    t.boolean "active", default: false
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

  create_table "body_parts", force: :cascade do |t|
    t.string "name"
    t.boolean "side_present", default: false
    t.integer "sort_order"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

  create_table "branches", force: :cascade do |t|
    t.integer "business_company_id"
    t.string "name"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

  create_table "business_companies", force: :cascade do |t|
    t.string "name"
    t.string "phone"
    t.integer "business_client_id"
    t.string "policy_number"
    t.string "policy_year"
    t.string "contact_person_name"
    t.string "contact_person_email"
    t.string "contact_person_phone"
    t.text "notepad"
    t.string "broker_of_record"
    t.integer "who_to_bill"
    t.integer "payment_options"
    t.string "medical_clinic"
    t.string "insurance_company"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

  create_table "claimants", force: :cascade do |t|
    t.string "work_status"
    t.string "follow_up_appointment"
    t.string "name"
    t.string "phone"
    t.string "date_of_birth"
    t.string "occupation"
    t.string "last_day_worked"
    t.float "body_mass_index"
    t.string "claim_litigated"
    t.string "start_date"
    t.string "open_date"
    t.string "closed_date"
    t.string "claim_type"
    t.string "body_part"
    t.string "cause_of_loss"
    t.string "type_of_injury"
    t.string "work_status_report"
    t.string "claim_number"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

  create_table "departments", force: :cascade do |t|
    t.integer "business_company_id"
    t.string "name"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

  create_table "images", force: :cascade do |t|
    t.string "image"
    t.integer "imageable_id"
    t.string "imageable_type"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

  create_table "injury_types", force: :cascade do |t|
    t.string "name"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

  create_table "invoice_details", force: :cascade do |t|
    t.integer "invoice_id"
    t.integer "invoiceable_id"
    t.string "invoiceable_type"
    t.text "description"
    t.float "tax"
    t.float "price"
    t.integer "quantity"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

  create_table "invoices", force: :cascade do |t|
    t.integer "user_id"
    t.string "number"
    t.integer "status"
    t.integer "payment_status"
    t.float "price"
    t.float "tax"
    t.float "total_amount"
    t.boolean "archive", default: false
    t.boolean "offline_paid", default: false
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

  create_table "medical_clinics", force: :cascade do |t|
    t.integer "business_company_id"
    t.string "name"
    t.string "phone_number"
    t.integer "contact_name"
    t.integer "branch_id"
    t.integer "user_id"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

  create_table "payment_profiles", force: :cascade do |t|
    t.integer "business_company_id"
    t.string "card_name"
    t.string "card_number"
    t.string "card_expiry_date"
    t.string "token"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

  create_table "payment_transactions", force: :cascade do |t|
    t.boolean "payment_status", default: false
    t.string "response_message"
    t.string "third_party_payment_id"
    t.integer "payable_id"
    t.string "payable_type"
    t.integer "payment_method_id"
    t.boolean "refund_status", default: false
    t.string "refund_transaction_id"
    t.float "refunded_amount"
    t.string "commission_transaction_id"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

  create_table "roles", force: :cascade do |t|
    t.string "name"
    t.string "resource_type"
    t.bigint "resource_id"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.index ["name", "resource_type", "resource_id"], name: "index_roles_on_name_and_resource_type_and_resource_id"
    t.index ["name"], name: "index_roles_on_name"
    t.index ["resource_type", "resource_id"], name: "index_roles_on_resource_type_and_resource_id"
  end

  create_table "tasks", force: :cascade do |t|
    t.string "name"
    t.date "task_date"
    t.date "completion_date"
    t.text "description"
    t.boolean "is_pending"
    t.integer "employee_id"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

  create_table "users", force: :cascade do |t|
    t.string "first_name"
    t.string "last_name"
    t.string "phone"
    t.string "username"
    t.string "email", default: "", null: false
    t.string "encrypted_password", default: "", null: false
    t.string "type"
    t.string "reset_password_token"
    t.datetime "reset_password_sent_at"
    t.datetime "remember_created_at"
    t.integer "sign_in_count", default: 0, null: false
    t.datetime "current_sign_in_at"
    t.datetime "last_sign_in_at"
    t.inet "current_sign_in_ip"
    t.inet "last_sign_in_ip"
    t.string "confirmation_token"
    t.datetime "confirmed_at"
    t.datetime "confirmation_sent_at"
    t.string "unconfirmed_email"
    t.integer "failed_attempts", default: 0, null: false
    t.string "unlock_token"
    t.datetime "locked_at"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.index ["email"], name: "index_users_on_email", unique: true
    t.index ["reset_password_token"], name: "index_users_on_reset_password_token", unique: true
  end

  create_table "users_roles", id: false, force: :cascade do |t|
    t.bigint "user_id"
    t.bigint "role_id"
    t.index ["role_id"], name: "index_users_roles_on_role_id"
    t.index ["user_id", "role_id"], name: "index_users_roles_on_user_id_and_role_id"
    t.index ["user_id"], name: "index_users_roles_on_user_id"
  end

end
